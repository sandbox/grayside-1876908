# #Heading

This module provides special wrapping to HTML headings in content.

  * Text Filter that adds an ID attribute for anchored links and a link to easily access such a link.
  * Styling that makes the latter link appear only on hover.

